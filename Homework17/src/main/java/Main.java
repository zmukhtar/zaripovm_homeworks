//На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.
//
//        Вывести:
//        Слово - количество раз
//
//        Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        String text = "Hello World Hello Marsel Hello I'am Hello";
        String[] wordString = text.split(" ");

        Map<String, Integer> map = new HashMap<>();

        for (int i = 0; i <wordString.length ; i++) {

            if( map.containsKey(wordString[i])){
                int countWord= map.get(wordString[i]);
                map.put(wordString[i],countWord+1);
            }
            else map.put(wordString[i], 1);

        }
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        for( Map.Entry<String,Integer> entry: entries){
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }




    }

}
