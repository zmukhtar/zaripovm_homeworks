public class Circle extends Ellipse implements Moving {



    public Circle(int a, int b, int coordinateX, int coordinateY) {
        super(a, b);

    }
    @Override
    public int getPerimeter(){
        setPerim((int) ((double)2 * 3.14 * Math.sqrt((getA() * getA()))));
        System.out.println("Периметр Круга = " + getPerim());
        return getPerim();
    }


    @Override
    public void inMoving(int x, int y) {

        setX(x);
        setY(y);
        System.out.println("Круг перемещен в координату"+ getX()+" : " + getY());

    }
}
