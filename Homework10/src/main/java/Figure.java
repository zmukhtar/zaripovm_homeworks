public abstract class Figure {
    private int a;
    private int b;
    private int x;
    private int y;
    private int perim;

    public  static  final int COORDINATE_X=0;
    public  static  final int COORDINATE_Y=0;



    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
    public int getPerim() {return perim;}
    public void setPerim(int perim) {this.perim = perim;}
    public int getA() {return a;}
    public void setA(int a) {this.a = a;}
    public int getB() {return b;}
    public void setB(int b) {this.b = b;}

    public Figure(int a, int b) {
        this.a = a;
        this.b = b;
    }
    public abstract int getPerimeter();

    public  abstract void moovingInCoordinate();
}
