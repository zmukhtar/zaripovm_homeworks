public class Square extends Rectangle implements Moving{


    public Square(int x, int y, int coordinateX, int coordinateY) {
        super(x, y);

    }
    @Override
    public int getPerimeter(){

        setPerim(getA()*4);
        System.out.println("Периметр Квадрата = " + getPerim());
        return getPerim();
    }

    @Override
    public void inMoving(int x, int y) {

        setX(x);
        setY(y);
        System.out.println("Квадрат перемещен в координату"+ getX()+" : " + getY());

    }

}