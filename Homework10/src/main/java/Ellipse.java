public class Ellipse extends Figure {

    public Ellipse(int a, int b) {
        super(a, b);
    }
    @Override
    public int getPerimeter() {
        setPerim((int) ((double) 2 * 3.14 * Math.sqrt((getA() * getA() + getB() * getB()) / 2)));
        System.out.println("Периметр Эллипса = " + getPerim());
        return getPerim();


    }

    @Override
    public void moovingInCoordinate() {
        System.out.println("Координаты фигуры перемещены в точку "+ COORDINATE_X+" : " + COORDINATE_Y );
        setX(COORDINATE_X);
        setY(COORDINATE_Y);
    }
}
