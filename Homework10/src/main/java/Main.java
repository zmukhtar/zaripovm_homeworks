//Сделать класс Figure из задания 09 абстрактным.
//
//        Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
//
//        Данный интерфейс должны реализовать только классы Circle и Square.
//
//        В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.

public class Main {
    public static void main(String[] args) {

//        Figure figure = new Figure(2, 3);

        Ellipse ellipse = new Ellipse(2, 3);
        Circle circle = new Circle(2,3,4,5);
        Rectangle rectangle = new Rectangle(2,3);
        Square square = new Square(2,3,4,5);

        Figure[] figures = new Figure[4];

//        figures[0] = Figure.figure;
        figures[0] = ellipse;
        figures[1] = circle;
        figures[2] = rectangle;
        figures[3] = square;
        circle.inMoving(12,14);
        square.inMoving(14,16);

        System.out.println("Круг находится на координате "+ circle.getX() +" : " + circle.getY());
        System.out.println("Квадрат находится на координате "+ square.getX() +" : " + square.getY());

        for (int i = 0; i < figures.length; i++) {
            figures[i].getPerimeter();

        }
        for (int i = 0; i < figures.length; i++) {
            System.out.println(figures[i].getClass());
            figures[i].moovingInCoordinate();

        }


    }
}
