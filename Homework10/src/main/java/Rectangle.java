public class Rectangle extends Figure {


    public Rectangle(int x, int y) {
        super(x, y);
    }

    @Override
    public int getPerimeter(){
        setPerim((getA()*2)+(getB()*2));
        System.out.println("Периметр Прямоугольника = " + getPerim());
        return getPerim();
    }

    @Override
    public void moovingInCoordinate() {
        System.out.println("Координаты фигуры перемещены в точку "+ COORDINATE_X+" : " + COORDINATE_Y );
        setX(COORDINATE_X);
        setY(COORDINATE_Y);
    }


}
