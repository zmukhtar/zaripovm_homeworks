//Предусмотреть функциональный интерфейс
//
//interface ByCondition {
//    boolean isOk(int number);
//}
//
//    Реализовать в классе Sequence метод:
//
//public static int[] filter(int[] array, ByCondition condition) {
//        ...
//        }
//
//        Данный метод возвращает массив, который содержит элементы, удовлетворяющиие логическому выражению в condition.
//
//        В main в качестве condition подставить:
//
//        - проверку на четность элемента
//        - проверку, является ли сумма цифр элемента четным числом.

public class Main {
    public static void main(String[] args) {

        ByCondition byConditionParity = new ByCondition() {

            //Проверка на четность

            @Override
            public boolean isOk(int number) {
                number %= 2;
                if(number==0) {
                    return true;
                }
                else {
                    return false;
                }
            }

        };
        ByCondition byConditionParitySummNumber = new ByCondition() {

            //Проверка на четность  суммы цифр числа

            @Override
            public boolean isOk(int number) {
                String numberString = Integer.toString(number);
                int[] array = new int[numberString.length()];
                int summ=0;

                for (int i = 0; i <array.length ; i++) {
                    array[i]=numberString.charAt(i);
                }
                for (int i = 0; i < array.length ; i++) {
                    summ +=array[i];
                }

                summ %=2;
                if(summ==0) {
                    return true;
                }
                else {
                    return false;
                }
            }
        };

        int[] arrayNumbers = {12,14,13,15,24,555};
        int[] arrayNumbersParity= new int[arrayNumbers.length];
        int[] arrayNumbersParitySumm= new int[arrayNumbers.length];

            System.out.println("Входящий массив.");
        for (int i = 0; i <arrayNumbers.length ; i++) {
            System.out.print(arrayNumbers[i]+", ");
        }
        System.out.println("");

        Sequence sequence = new Sequence();

        arrayNumbersParity = sequence.filter(arrayNumbers, byConditionParity);
        System.out.println("Массив четных чисел из входящего массива.");
        for (int i = 0; i <arrayNumbersParity.length ; i++) {
            System.out.print(arrayNumbersParity[i]+", ");
        }
        System.out.println("");

        arrayNumbersParitySumm=sequence.filter(arrayNumbers, byConditionParitySummNumber);
        System.out.println("Массив чисел в которых сумма цифр числа четная. ");
        for (int i = 0; i <arrayNumbersParity.length ; i++) {
            System.out.print(arrayNumbersParitySumm[i]+", ");
        }

    }
}
