public class Sequence {
    public int[] filter(int[] array, ByCondition condition) {

        int[] filterArray = new int[array.length];
        int indexNewParityNuber = 0;

        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                filterArray[indexNewParityNuber] = array[i];
                indexNewParityNuber++;
            }

        }
        return filterArray;
    }
}
