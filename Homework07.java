//
//        На вход подается последовательность чисел, оканчивающихся на -1.
//        Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
//        Гарантируется:
//        Все числа в диапазоне от -100 до 100.
//        Числа встречаются не более 2 147 483 647-раз каждое.
//        Сложность алгоритма - O(n)




import  java.util.Scanner;
class Homework07 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] array = new int[201];
        
        try{
        byte a = scanner.nextByte();
        int indexArray = 0;
//        На вход подается последовательность чисел, оканчивающихся на -1.
        if(a!=-1){
        while (a != -1 ) {
            

            if(a > -101 && a < 101){
            indexArray = a + 100;
            array[indexArray] += 1;
            
           
            }
            else{
                    System.out.println("Вы ввели число вне диапазона от -100 до 100");
            }
            a = scanner.nextByte();
        }
//        Сложность алгоритма - O(n)Такой сложностью обладает, например,
//        алгоритм поиска наибольшего элемента в не отсортированном массиве.
//        Нам придётся пройтись по всем n элементам массива, чтобы понять, какой из них максимальный.
        for (int i = 0; i < 200; i++) {
            int count = 0;
            if(array[i]!= 0){
                if (array[i]<array[indexArray]){

                    indexArray=i;
                }
            }
        }
        System.out.println("Минимальное количество раз встречается число "+(indexArray-100));
        
    }
    else
        System.out.println("Вы ничего не ввели");
    }
catch (Exception e) {
    System.out.println("Обработка исключений");
    System.out.println(e.getMessage());
    
    
}
 
}

}



