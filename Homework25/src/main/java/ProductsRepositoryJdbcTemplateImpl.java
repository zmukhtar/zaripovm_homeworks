import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository{

    private static final String SQL_SELECT_ALL = "select * from product order by id";
    private static final String SQL_SELECT_ALL_BY_PRICE = "select * from product where price =";
    private static final String SQL_SELECT_ALL_BY_ORDERS_COUNT = "select  count_product, (select description from product where id = orders.owner_id_product) from orders where count_product =";



    private JdbcTemplate jdbcTemplate;

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        int price = row.getInt("price");
        int count = row.getInt("count");

        return new Product(id, description, price, count);
    };
    private static final RowMapper<CountOrders> userRowMapper2 = (row, rowNumber) -> {

        String description = row.getString("description");
        int countProduct = row.getInt("count_product");
        return new CountOrders(description, countProduct);
    };



    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(int price) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_PRICE+price, userRowMapper);
    }

    @Override
    public List<CountOrders> findAllByOrdersCount(int ordersCount) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_ORDERS_COUNT+ordersCount, userRowMapper2);
    }
}
