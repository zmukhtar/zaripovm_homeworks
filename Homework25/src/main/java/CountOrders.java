import lombok.*;



    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public class CountOrders {
        private  String description;
        private int countOrders;

    }


