import java.util.List;

public interface ProductsRepository {

List<Product> findAll();
List<Product> findAllByPrice(int price);
List<CountOrders> findAllByOrdersCount(int ordersCount); //- найти все товары по количеству заказов, в которых участвуют

}
