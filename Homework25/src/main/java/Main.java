//Реализовать ProductsRepository
//
//        - List<Product> findAll();
//        - List<Product> findAllByPrice(double price);
//        * List<Product> findAllByOrdersCount(int ordersCount); - найти все товары по количеству заказов, в которых участвуют

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

public class Main {
    public static void main(String[] args) {
        DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://185.148.218.4:5432/postgres",
                "postgres", "1");

        ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);

        System.out.println(productsRepository.findAll());

        System.out.println(productsRepository.findAllByPrice(93));

        System.out.println(productsRepository.findAllByOrdersCount(78));

    }
}
