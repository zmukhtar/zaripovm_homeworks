//Подготовить файл с записями, имеющими следующую структуру:
//        [НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]
//
//        o001aa111|Camry|Black|133|82000
//        o002aa111|Camry|Green|133|0
//        o001aa111|Camry|Black|133|82000
//
//        Используя Java Stream API, вывести:
//
//        Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
//        Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
//        * Вывести цвет автомобиля с минимальной стоимостью. // min + map
//        * Среднюю стоимость Camry *
//
//        https://habr.com/ru/company/luxoft/blog/270383/
//
//        Достаточно сделать один из вариантов.

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Data data = new Data();
        try (BufferedReader reader = new BufferedReader(new FileReader("data.txt"))) {
            reader.lines()
                    .map(line ->{
                        String[] dataLines =line.split("\\|");
                        data.setNumber(dataLines[0]);
                        data.setModel(dataLines[1]);
                        data.setColor(dataLines[2]);
                        data.setSpeed(Integer.parseInt(dataLines[3]));
                        data.setPrice(Integer.parseInt(dataLines[4]));
                    return data;
                    })
                    .filter(line -> ((line.getColor().equals("Black"))||(line.getSpeed()==0)))
                    .forEach(line -> System.out.println( line.getNumber()+" "+ line.getModel()+" "+line.getColor()+" "+
                            line.getSpeed()+" "+line.getPrice()));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }


    }
}
