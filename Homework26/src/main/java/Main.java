import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
            try {
                Scanner sc = new Scanner(System.in);
                NumbersUtil numbersUtil = new NumbersUtil();

                int nod = numbersUtil.gcd(Integer.valueOf(sc.nextLine()),Integer.valueOf(sc.nextLine()));
                System.out.println("Наибольший общий делитель: " + nod);
            } catch (NumberFormatException | UnsupportedOperationException e) {
                System.out.println(e.getMessage());
            }

        }


}
