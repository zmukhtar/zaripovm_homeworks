public class NumbersUtil {
    /*
      нод(18, 12) -> 6
      нод(9, 12) -> 3
      нод(64, 48) -> 16

      Предусмотреть, когда на вход "некрсивые числа", отрицательные числа -> исключения
       */
    public int gcd(int a, int b) {
        if (a<=0 || b<=0) throw new IllegalArgumentException();
        while(a!=0 && b!=0){
            if (a>b) a=a%b;
            else b=b%a;
        }
        return a+b;
    }

}
