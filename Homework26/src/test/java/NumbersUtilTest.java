import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
public class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();


    @Nested
    @DisplayName("sum() is working")
    public class ForSum {
        @ParameterizedTest(name = "return {2} on a = {0} and b = {1}")
        @CsvSource(value = {"10, 2, 2", "32, 24, 8", "15, 10, 5"})
        public void return_correct_sum(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }
        @ParameterizedTest(name = "throws exception on a = {0} and b = {1}")
        @CsvSource(value = {"0, 10","10, 0","-2, 10", "10, -2"})
        public void bad_numbers_throws_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }
    }
}
