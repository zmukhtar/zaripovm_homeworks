import java.util.Scanner;

import javax.sound.sampled.SourceDataLine;
class Homework05 {
    /*Реализовать программу на Java, которая для последовательности чисел, оканчивающихся на -1 выведет самую минимальную цифру, встречающуюся среди чисел последовательности.

Например:

345
298
456
-1

Ответ: 2
*/

    public static void main(String args[]) {
            Scanner scanner = new Scanner(System.in);
// объявляем переменные
            int number =scanner.nextInt();
            int[] localData = new int[100];
            int countEnterData=0;


// циклом считываем данные введенные пользователем и записываем в масссив b
            int i=0;
            while(number != -1){

                localData[i] = number;
                i++;
                countEnterData++;
                number=scanner.nextInt();
            }
//
            int min=10;
            String a ="";
            int c;
            //запускаем цикл по количеству введенных данных пользователем
            for(int j=0; j<countEnterData; j++){
                // переводим введенное число в тип String
                a = Integer.toString(localData[j]);
                // запускаем цикл в котором проходим по цифрам числа 
                for(int k=0; k<a.length(); k++) {
                    c=Character.getNumericValue(a.charAt(k));
                    if(c<min){
                        min = c;
                    }
                }
            }
        
            if (min==10){
                System.out.println("Вы не ввели ничего");
            }
            else{
                System.out.println("Ответ " + min);
            }
           
        }
    }
