public class SumThread extends Thread {
    private int from;
    private int to;
    private static int count;

    public SumThread(int from, int to) {
        this.from = from;
        this.to = to;
        Thread thread = new Thread(this);
        thread.start();
        try{
            thread.join();
        }
        catch (InterruptedException e){
            throw new IllegalArgumentException(e);
        }
    }
    @Override
    public void run(){
        try {
        int sum = 0;
        for( int i = from; i <= to; i++){
            sum += Main.array[i];
        }

            Main.sums[count] = sum;
            count++;
        } catch (ArrayIndexOutOfBoundsException ignore) {}
    }
}
