import java.util.Random;
import java.util.Scanner;


public class Main {

    public static int array[];
    public static int sums[];


    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int numbersCount = scanner.nextInt();
        System.out.println("Количество потоков: ");
        int threadsCount = scanner.nextInt();

        // инициализируем массивы
        array = new int[numbersCount];
        sums = new int[threadsCount];

        // заполняем случайными числами
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }

        // инициализируем переменные для подсчёта сумм и подсчёт сумм через потоки
        int realSum = 0;
        int byThreadSum = 0;


        /* for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        // для 2 000 000 -> 98996497, 98913187
        System.out.println(realSum); */

        // TODO: реализовать работу с потоками
        int countOfElements = numbersCount / threadsCount - 1;
        int residue = numbersCount % threadsCount;
        for (int i = 0; i < array.length; i += (countOfElements + residue)) {
        new SumThread( i, i + countOfElements + residue);
        i += residue;
        residue = 0;
     }

        for (int i = 0; i < array.length; i++) {
            realSum += array[i];
        }

        for (int i = 0; i < threadsCount; i++) {
            byThreadSum += sums[i];
        }


        System.out.println(realSum);
        System.out.println(byThreadSum);
    }
}
