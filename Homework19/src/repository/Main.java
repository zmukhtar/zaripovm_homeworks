package repository;

import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();
        List<User> users1 = usersRepository.findByAge(33);
        List<User> users2 = usersRepository.findByIsWorkerIsTrue();
        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println("Print users method findByAge");
        for (User user : users1) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        System.out.println("Print users method findByIsWorkerIsTrue");
        for (User user : users2) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
        //User user = new User("Игорь", 33, true);
       // usersRepository.save(user);
    }
}
