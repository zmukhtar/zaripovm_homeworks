import java.util.Scanner;

public class Raschet {
    public Human[] sumHumman(Human[] humans) {
        //На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
        Scanner scanner = new Scanner(System.in);


        //Считать эти данные в массив объектов.
        for (int i = 0; i < 10; i++) {
            humans[i] = new Human();

            System.out.println("Введите имя " + (i + 1) + "человека");
            humans[i].setName(scanner.next());

            System.out.println("Введите вес " + (i + 1) + "человека");
            humans[i].setWeight(scanner.nextFloat());
        }
        return humans;
    }

    public Human[] sortHumman(Human[] humans) {

// циклический поиск последовательности минимальных весов
        //проходим по массиву и находя индекс минимального значения записываем их значения в массив от минимума к максимуму
        //запускаем цикл для перезаписи массива от минимального значения до максимального
        for (int i = 0; i < 10; i++) {
            //присваивам переменной min вес текущего индекса массива
            float min = humans[i].getWeight();
            // переменной minIndex присваиваем значение текущего прохода цикла
            int minIndex = i;
            //проходим по массиву и находим индекс минимального значения веса
            for (int j = i + 1; j < 10; j++) {

                if (humans[j].getWeight() < min) {
                    min = humans[j].getWeight();
                    minIndex = j;
                }
            }
            //присваиваем переменным temp  и temp2 значения под индексами массива текущего прохода цикла
            float temp = humans[i].getWeight();
            String temp2 = humans[i].getName();
            //на место  значений  за индексом прохода цикла присваиваем значения за индексом следующего минимального веса
            humans[i].setWeight(humans[minIndex].getWeight());
            humans[i].setName(humans[minIndex].getName());
            //а на их место вставляем значение текущего прохода цикла
            humans[minIndex].setWeight(temp);
            humans[minIndex].setName(temp2);
        }

        return humans;
    }


}
