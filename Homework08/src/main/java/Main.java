//На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
//Считать эти данные в массив объектов.
//Вывести в отсортированном по возрастанию веса порядке.
import  java.util.Scanner;
public class Main {
    public static void main(String[] args) {

        Human[] humans = new Human[10];
        Raschet rashet =new Raschet();
//На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
        rashet.sumHumman(humans);
//Считать эти данные в массив объектов.
//Вывести в отсортированном по возрастанию веса порядке.
        rashet.sortHumman(humans);
//вывести результат на экран
        for (int i = 0; i < 10; i++) {
            System.out.println(humans[i].getName() + "  " + humans[i].getWeight());
        }


    }

}
